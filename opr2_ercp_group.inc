<?php

function opr2_ercp_group($name, $bundle) {
  switch ($name) {
  case 'group_inline':
    return _opr2grp('inline');

  case 'group_indication':
    return _opr2grp('category', '2. Indikáció és tervezett beavatkozás');
  case 'group_asa':
    return _opr2grp('category', '3. ASA Score');

  case 'group_therapy_aac':
    return _opr2grp('category', '4. Aggregáció gátló és/vagy antikoaguláns kezelés');
  case 'group_therapy_aspirin':
  case 'group_therapy_aspirin_dc':
  case 'group_therapy_clopidogrel':
  case 'group_therapy_clopidogrel_dc':
  case 'group_therapy_prasugrel':
  case 'group_therapy_prasugrel_dc':
  case 'group_therapy_noac':
  case 'group_therapy_noac_dc':
  case 'group_therapy_kva':
  case 'group_therapy_kva_dc':
  case 'group_therapy_lmwh':
  case 'group_therapy_lmwh_dc':
    return _opr2grp('cond_hide', 'therapy');

  case 'group_therapy_coag':
    return _opr2grp('category', '5. Véralvadási zavar');
  case 'group_therapy_coag_inr':
  case 'group_therapy_coag_tct':
  case 'group_therapy_coag_hemo':
    return _opr2grp('cond_hide', 'therapy');

  case 'group_therapy_sedmed':
    return _opr2grp('category', '6. Szedáció és gyógyszeres profilaxis');
  case 'group_therapy_sed':
  case 'group_therapy_sed_wake':
  case 'group_therapy_sed_prop':
  case 'group_therapy_sed_other':
  case 'group_therapy_sed_mon':
  case 'group_therapy_sed_adot':
  case 'group_therapy_pepp':
  case 'group_therapy_pepp_ind':
  case 'group_therapy_pepp_dic':
  case 'group_therapy_pepp_iv':
  case 'group_therapy_pepp_other':
  case 'group_therapy_abpr':
    return _opr2grp('cond_hide', '');

  case 'group_examtime':
    return _opr2grp('category', '7. Vizsgálati idő');

  case 'group_anatomy':
    return _opr2grp('category', '8. Anatómia');
  case 'group_anatomy_pastest':
    return _opr2grp('cond_hide', '');

  case 'group_cannul':
    return _opr2grp('category', '9. Kanülálás');
  case 'group_cannul_biliary':
  case 'group_cannul_biliary_sup':
  case 'group_cannul_biliary_deep':
  case 'group_cannul_biliary_precut':
  case 'group_cannul_biliary_pwir':
  case 'group_cannul_biliary_pst':
  case 'group_cannul_pancmaj':
  case 'group_cannul_pancmaj_sup':
  case 'group_cannul_pancmaj_deep':
  case 'group_cannul_pancmaj_precut':
  case 'group_cannul_pancmin':
  case 'group_cannul_pancmin_sup':
  case 'group_cannul_pancmin_deep':
  case 'group_cannul_pancmin_precut':
    return _opr2grp('cond_hide', '');

  case 'group_cpgraph':
    return _opr2grp('category', '10. Cholangiographia és pancreatographia lelete');
  case 'group_cpgraph_bil':
  case 'group_cpgraph_bil_stone':
  case 'group_cpgraph_bil_sten':
  case 'group_cpgraph_bil_flux':
  case 'group_cpgraph_panc':
  case 'group_cpgraph_panc_sten':
  case 'group_cpgraph_panc_pcyst':
    return _opr2grp('cond_hide', '');

  case 'group_therapy':
    return _opr2grp('category', '11. Terápia');
  case 'group_therapy_sphin':
  case 'group_therapy_sphin_precut':
  case 'group_therapy_dil':
  case 'group_therapy_dil_pbal':
  case 'group_therapy_dil_sten':
  case 'group_therapy_stonex':
    return _opr2grp('cond_hide', '');

  case 'group_proposal':
    return _opr2grp('category', '12. További kezelési / kivizsgálási javaslat');
  case 'group_proposal_iv':
  case 'group_proposal_peros':
  case 'group_proposal_ab':
  case 'group_proposal_acoag':
  case 'group_proposal_lab':
  case 'group_proposal_imag':
  case 'group_proposal_consil':
    return _opr2grp('cond_hide', '');

  case 'group_complications':
    return _opr2grp('category', '13. Szövődmények és ellátásuk');
  case 'group_comp_immed':
  case 'group_comp_immed_bleed':
  case 'group_comp_immed_perf':
  case 'group_comp_immed_carp':
  case 'group_comp_late':
  case 'group_comp_late_bleed':
  case 'group_comp_late_bleed_tf':
  case 'group_comp_late_perf':
  case 'group_comp_late_panc':
  case 'group_comp_late_chola':
  case 'group_comp_late_chocy':
  case 'group_comp_after':
  case 'group_comp_after_mort':
    return _opr2grp('cond_hide', '');

  case 'group_diff':
    return _opr2grp('category', '14. A vizsgálat nehézsége');
  }
}

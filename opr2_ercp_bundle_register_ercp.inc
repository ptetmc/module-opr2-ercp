<?php

function opr2_ercp_bundle_register_ercp() {
  $bundle = array(
    'machine_name' => 'register_ercp',
    'instances' => array(
      'field_form_version' => array(
        'default_value' => array(array('value'=>'v1')),
      ),
    ), // end instances

    'groups' => array(
    ), // end groups

    'tree' => array(
      'field_form_version',

      'field_inpatient',

      'group_inline' => array(
        'group_indication' => array(
          'field_indication_ercp',
          'field_indication_therapy',
          'field_indication_other',
        ),

        'group_asa' => array(
          'field_asa',
        ),

        'group_therapy_aac' => array(
          'group_therapy_aspirin' => array(
            'field_therapy_aspirin',
            'field_therapy_aspirin_dose',
            'group_therapy_aspirin_dc' => array(
              'field_therapy_aspirin_dc',
              'field_therapy_aspirin_dcd',
            ),
          ),

          'group_therapy_clopidogrel' => array(
            'field_therapy_clopidogrel',
            'group_therapy_clopidogrel_dc' => array(
              'field_therapy_clopidogrel_dc',
              'field_therapy_clopidogrel_dcd',
            ),
          ),

          'group_therapy_prasugrel' => array(
            'field_therapy_prasugrel',
            'field_therapy_prasugrel_dose',
            'group_therapy_prasugrel_dc' => array(
              'field_therapy_prasugrel_dc',
              'field_therapy_prasugrel_dcd',
            ),
          ),

          'group_therapy_noac' => array(
            'field_therapy_noac',
            'field_therapy_noac_name',
            'field_therapy_noac_dose',
            'group_therapy_noac_dc' => array(
              'field_therapy_noac_dc',
              'field_therapy_noac_dcd',
            ),
          ),

          'group_therapy_kva' => array(
            'field_therapy_kva',
            'field_therapy_kva_name',
            'field_therapy_kva_dose',
            'group_therapy_kva_dc' => array(
              'field_therapy_kva_dc',
              'field_therapy_kva_dcd',
            ),
          ),

          'group_therapy_lmwh' => array(
            'field_therapy_lmwh',
            'field_therapy_lmwh_name',
            'field_therapy_lmwh_dose',
            'field_therapy_lmwh_lastdate',
          ),
        ), // end group_therapy_aac

        'group_therapy_coag' => array(
          'group_therapy_coag_inr' => array(
            'field_therapy_coag_inr',
            'field_therapy_coag_inr_corr',
          ),
        ),

        'group_therapy_sedmed' => array(
          'group_therapy_sed' => array(
            'field_therapy_sed',
            'group_therapy_sed_wake' => array(
              'field_therapy_sed_wake',
              'field_therapy_sed_wake_what',
              'field_therapy_sed_wake_dose',
            ),
            'group_therapy_sed_prop' => array(
              'field_therapy_sed_prop',
              'field_therapy_sed_prop_dose',
            ),
            'group_therapy_sed_other' => array(
              'field_therapy_sed_other',
              'field_therapy_sed_other_what',
              'field_therapy_sed_other_dose',
            ),
            'group_therapy_sed_mon' => array(
              'field_therapy_sed_mon',
              'field_therapy_sed_mon_how',
            ),
            'group_therapy_sed_adot' => array(
              'field_therapy_sed_adot',
              'field_therapy_sed_adot_what',
              'field_therapy_sed_adot_dose',
            ),
          ),

          'group_therapy_pepp' => array(
            'field_therapy_pepp',
            'group_therapy_pepp_ind' => array(
              'field_therapy_pepp_ind',
              'field_therapy_pepp_ind_cc',
              'field_therapy_pepp_ind_when',
            ),
            'group_therapy_pepp_dic' => array(
              'field_therapy_pepp_dic',
              'field_therapy_pepp_dic_cc',
              'field_therapy_pepp_dic_when',
            ),
            'group_therapy_pepp_iv' => array(
              'field_therapy_pepp_iv',
              'field_therapy_pepp_iv_what',
              'field_therapy_pepp_iv_dose',
            ),
            'group_therapy_pepp_other' => array(
              'field_therapy_pepp_other',
              'field_therapy_pepp_other_what',
              'field_therapy_pepp_other_dose',
              'field_therapy_pepp_other_when',
            ),
          ),

          'group_therapy_abpr' => array(
            'field_therapy_abpr',
            'field_therapy_abpr_type',
            'field_therapy_abpr_when',
            'field_therapy_abpr_dose',
            'field_therapy_abpr_len',
          ),
        ),

        'group_examtime' => array(
          'field_examtime_start',
          'field_examtime_can',
          'field_examtime_duodiff',
          'field_examtime_end',
          'field_examtime_fluo',
          'field_examtime_fluo_dose',
        ),

        'group_anatomy' => array(
          'field_anatomy_opstop',
          'field_anatomy_deform',
          'field_anatomy_vaterpap',
          'group_anatomy_pastest' => array(
            'field_anatomy_pastest',
            'field_anatomy_pastest_res',
            'field_anatomy_juxtapdiv',
            'field_anatomy_orivis',
            'field_anatomy_pappos',
          ),
          'field_anatomy_sampling',
        ),

        'group_cannul' => array(
          'group_cannul_biliary' => array(
            'field_cannul_biliary',
            'group_cannul_biliary_sup' => array(
              'field_cannul_biliary_sup',
              'field_cannul_biliary_sup_type',
            ),
            'group_cannul_biliary_deep' => array(
              'field_cannul_biliary_deep',
              'field_cannul_biliary_deep_type',
            ),
            'group_cannul_biliary_precut' => array(
              'field_cannul_biliary_precut',
              'field_cannul_biliary_precut_type',
            ),
            'group_cannul_biliary_pwir' => array(
              'field_cannul_biliary_pwir',
              'field_cannul_biliary_pwir_type',
            ),
            'group_cannul_biliary_pst' => array(
              'field_cannul_biliary_pst',
              'field_cannul_biliary_pst_type',
            ),
            'field_cannul_biliary_rendez',
            'field_cannul_biliary_fail',
            'field_cannul_biliary_pancduct',
            'field_cannul_biliary_pancins',
          ),

          'group_cannul_pancmaj' => array(
            'field_cannul_pancmaj',
            'group_cannul_pancmaj_sup' => array(
              'field_cannul_pancmaj_sup',
              'field_cannul_pancmaj_sup_type',
            ),
            'group_cannul_pancmaj_deep' => array(
              'field_cannul_pancmaj_deep',
              'field_cannul_pancmaj_deep_type',
            ),
            'group_cannul_pancmaj_precut' => array(
              'field_cannul_pancmaj_precut',
              'field_cannul_pancmaj_precut_type',
            ),
            'field_cannul_pancmaj_fail',
          ),

          'group_cannul_pancmin' => array(
            'field_cannul_pancmin',
            'group_cannul_pancmin_sup' => array(
              'field_cannul_pancmin_sup',
              'field_cannul_pancmin_sup_type',
            ),
            'group_cannul_pancmin_deep' => array(
              'field_cannul_pancmin_deep',
              'field_cannul_pancmin_deep_type',
            ),
            'group_cannul_pancmin_precut' => array(
              'field_cannul_pancmin_precut',
              'field_cannul_pancmin_precut_type',
            ),
            'field_cannul_pancmin_fail',
          ),

          'field_cannul_contrastex',
        ), // end group_cannul

        'group_cpgraph' => array(
          'group_cpgraph_bil' => array(
            'field_cpgraph_bil',
            'field_cpgraph_bil_dil',
            'field_cpgraph_bil_cal',
            'group_cpgraph_bil_stone' => array(
              'field_cpgraph_bil_stone',
              'field_cpgraph_bil_stone_size',
              'field_cpgraph_bil_stone_num',
              'field_cpgraph_bil_stone_loc',
            ),
            'field_cpgraph_bil_sludge',
            'field_cpgraph_bil_pus',
            'group_cpgraph_bil_sten' => array(
              'field_cpgraph_bil_sten',
              'field_cpgraph_bil_sten_loc',
              'field_cpgraph_bil_sten_len',
              'field_cpgraph_bil_sten_mal',
              'field_cpgraph_bil_sten_sam',
            ),
            'group_cpgraph_bil_flux' => array(
              'field_cpgraph_bil_flux',
              'field_cpgraph_bil_flux_loc',
            ),
          ),
          'group_cpgraph_panc' => array(
            'field_cpgraph_panc',
            'field_cpgraph_panc_dil',
            'field_cpgraph_panc_cal',
            'field_cpgraph_panc_wir',
            'group_cpgraph_panc_sten' => array(
              'field_cpgraph_panc_sten',
              'field_cpgraph_panc_sten_loc',
              'field_cpgraph_panc_sten_len',
              'field_cpgraph_panc_sten_mal',
              'field_cpgraph_panc_sten_sam',
            ),
            'group_cpgraph_panc_pcyst' => array(
              'field_cpgraph_panc_pcyst',
              'field_cpgraph_panc_pcyst_loc',
              'field_cpgraph_panc_pcyst_size',
            ),
          ),
        ), // end group_cpgraph

        'group_therapy' => array(
          'group_therapy_sphin' => array(
            'field_therapy_sphin',
            'group_therapy_sphin_precut' => array(
              'field_therapy_sphin_precut',
              'field_therapy_sphin_precut_type',
            ),
            'field_therapy_sphin_reg',
            'field_therapy_sphin_repap',
            'field_therapy_sphin_psph',
            'field_therapy_sphin_dsph',
          ),
          'group_therapy_dil' => array(
            'field_therapy_dil',
            'group_therapy_dil_pbal' => array(
              'field_therapy_dil_pbal',
              'field_therapy_dil_pbal_size',
              'field_therapy_dil_pbal_bsize',
            ),
            'group_therapy_dil_sten' => array(
              'field_therapy_dil_sten',
              'field_therapy_dil_sten_type',
              'field_therapy_dil_sten_size',
              'field_therapy_dil_sten_bsize',
            ),
          ),
          'group_therapy_stonex' => array(
            'field_therapy_stonex',
            'field_therapy_stonex_dorm',
            'field_therapy_stonex_bal',
            'field_therapy_stonex_mlit',
            'field_therapy_stonex_free',
          ),
        ), // end group_therapy

        'group_proposal' => array(
          'group_proposal_iv' => array(
            'field_proposal_iv',
            'field_proposal_iv_amount',
          ),
          'group_proposal_peros' => array(
            'field_proposal_peros',
            'field_proposal_peros_type',
          ),
          'group_proposal_ab' => array(
            'field_proposal_ab',
            'field_proposal_ab_type',
          ),
          'group_proposal_acoag' => array(
            'field_proposal_acoag',
            'field_proposal_acoag_date',
          ),
          'group_proposal_lab' => array(
            'field_proposal_lab',
            'field_proposal_lab_type',
            'field_proposal_lab_date',
          ),
          'group_proposal_imag' => array(
            'field_proposal_imag',
            'field_proposal_imag_type',
          ),
          'group_proposal_consil' => array(
            'field_proposal_consil',
            'field_proposal_consil_type',
          ),
        ),

        'group_complications' => array(
          'group_comp_immed' => array(
            'field_comp_immed',

            'group_comp_immed_bleed' => array(
              'field_comp_immed_bleed',
              'field_comp_immed_bleed_hs',
              'field_comp_immed_bleed_stop',
            ),
            'group_comp_immed_perf' => array(
              'field_comp_immed_perf',
              'field_comp_immed_perf_type',
              'field_comp_immed_perf_air',
              'field_comp_immed_perf_ther',
            ),
            'group_comp_immed_carp' => array(
              'field_comp_immed_carp',
              'field_comp_immed_carp_type',
              'field_comp_immed_carp_ther',
            ),
          ),
          'group_comp_late' => array(
            'field_comp_late',

            'group_comp_late_bleed' => array(
              'field_comp_late_bleed',
              'field_comp_late_bleed_det',
              'field_comp_late_bleed_exam',
              'field_comp_late_bleed_hs',
              'field_comp_late_bleed_stop',

              'group_comp_late_bleed_tf' => array(
                'field_comp_late_bleed_tf',
                'field_comp_late_bleed_tf_type',
                'field_comp_late_bleed_tf_amt',
              ),
            ),
            'group_comp_late_perf' => array(
              'field_comp_late_perf',
              'field_comp_late_perf_type',
              'field_comp_late_perf_det',
              'field_comp_late_perf_air',
              'field_comp_late_perf_ther',
            ),
            'group_comp_late_panc' => array(
              'field_comp_late_panc',
              'field_comp_late_panc_pep',
              'field_comp_late_panc_sev',
            ),
            'group_comp_late_chola' => array(
              'field_comp_late_chola',
              'field_comp_late_chola_det',
              'field_comp_late_chola_ther',
            ),
            'group_comp_late_chocy' => array(
              'field_comp_late_chocy',
              'field_comp_late_chocy_det',
              'field_comp_late_chocy_ther',
            ),
          ),
          'group_comp_after' => array(
            'field_comp_after',

            'group_comp_after_mort' => array(
              'field_comp_after_mort',
              'field_comp_after_mort_date',
              'field_comp_after_mort_conn',
            ),
            'field_comp_after_ther',
          ),
          'field_comp_sev',
        ),

        'group_diff' => array(
          'field_diff_obj',
          'field_diff_subj',
        ),
      ),
    ), // end tree
  );

  return $bundle;
}

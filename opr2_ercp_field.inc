<?php

function opr2_ercp_field($name) {
  switch ($name) {
  case 'field_indication_ercp':
    return _opr2fld('number_list', '2A ERCP indikációja', array(
      'options' => array(
        1 => 'Obstrukciós icterus',
        'Epevezeték betegsége',
        'Hasnyálmirigy vezeték betegsége',
        'Pancreas malignitás gyanúja, ha az egyéb képalkotók nem egyértelműek vagy normálisak',
        'Ismeretlen etiológiájú pancreatitis',
        'Akut biliaris pancreatitis',
        'Krónikus pancreatitis / pseudocysta preoperativ értékelése',
        'SOD manometria',
      ),
    ));
  case 'field_indication_therapy':
    return _opr2fld('number_list', '2B Terápiás Beavatkozás indikációja', array(
      'options' => array(
        'EST végzése:' => array(
          1 => 'Epeútkő',
          'Papilla stenosis / SOD',
          'Epeúti stent behelyezése',
          'Epeúti szűkület tágítása',
          'Sump sy.',
          'Choledochocele',
          'Vater papilla cc, ha sebészet nem jön szóba',
          'Pancreas vezeték elérésének elősegítése',
        ),
        'Epeúti stent behelyezése:' => array(
          10 => 'Benignus szűkület',
          'Malignus szűkület',
          'Ismeretlen természetű szűkület',
          'Fistula',
          'Post-operativ epecsorgás',
          'Nem eltávolítható, nagy epeúti kő',
        ),
        20 => 'Szűkület tágítása',
        'Papilla ballon dilatációja',
        'Nasobiliaris drain behelyezése',
        'Pancreas pseudocysta drainage',
        'Mintavétel pancreasvezetékből ',
        'Mintavétel epevezetékből',
        'Ampullectomia',
        'Cholangioscopia',
        'Pancreatoscopia',
      ),
    ));
  case 'field_indication_other':
    return _opr2fld('textarea', 'Egyéb epeúti vagy pancreas terápiás beavatkozások');

  case 'field_asa':
    return _opr2fld('number_list', 'ASA', array(
      'options' => array(
        1 => 'I. Az operálandó betegségtől eltekintve egyébként egészséges egyén',
        'II. A beteg tevékenységét nem befolyásoló enyhefokú szisztémás megbetegedés',
        'III. Súlyosabb szisztémás megbetegedés, mely a beteget a normális élettevékenységében befolyásolja',
        'IV. A beteg életét veszélyeztető súlyos szisztémás kórfolyamat',
        'V. Moribund beteg nagy 24 órás halálozási kockázattal',
      ),
    ));
  case 'field_therapy_aspirin':
    return _opr2fld('boolean_radio', 'aspirin');
  case 'field_therapy_clopidogrel':
    return _opr2fld('boolean_radio', 'clopidogrel');
  case 'field_therapy_prasugrel':
    return _opr2fld('boolean_radio', 'prasugrel');
  case 'field_therapy_noac':
    return _opr2fld('boolean_radio', 'NOAC');
  case 'field_therapy_kva':
    return _opr2fld('boolean_radio', 'K-vitamin antagonista');
  case 'field_therapy_lmwh':
    return _opr2fld('boolean_radio', 'LMWH');
  case 'field_therapy_aspirin_dose':
  case 'field_therapy_clopidogrel_dose':
  case 'field_therapy_prasugrel_dose':
  case 'field_therapy_noac_dose':
  case 'field_therapy_kva_dose':
  case 'field_therapy_lmwh_dose':
    return _opr2fld('number_decimal', 'milyen dózisban', array(
      'suffix' => 'mg',
    ));
  case 'field_therapy_aspirin_dc':
  case 'field_therapy_clopidogrel_dc':
  case 'field_therapy_prasugrel_dc':
  case 'field_therapy_noac_dc':
  case 'field_therapy_kva_dc':
    return _opr2fld('boolean_radio', 'felfüggesztés történt-e');
  case 'field_therapy_aspirin_dcd':
  case 'field_therapy_clopidogrel_dcd':
  case 'field_therapy_prasugrel_dcd':
  case 'field_therapy_noac_dcd':
  case 'field_therapy_kva_dcd':
    return _opr2fld('number_list', 'hány napja', array(
      'options' => array(
        1 => '1 nap',
        2 => '2 nap',
        3 => '3 nap',
        4 => '4 nap',
        5 => '5 nap',
        6 => '6 nap',
        7 => '1 hét',
        14 => '2 hét',
        21 => '3 hét',
        30 => '1 hónap',
        60 => '2 hónap',
        90 => '3 hónap',
        120 => '4 hónap',
        150 => '5 hónap',
        180 => '6 hónap',
        365 => '6-12 hónap',
        366 => 'több, mint 1 év',
      ),
    ));
  case 'field_therapy_noac_name':
  case 'field_therapy_kva_name':
  case 'field_therapy_lmwh_name':
    return _opr2fld('text', 'melyik');
  case 'field_therapy_lmwh_lastdate':
    return _opr2fld('date_which_day', 'utolsó dózis ideje');

  case 'field_therapy_coag_inr':
    return _opr2fld('number_decimal', 'INR érték');
  case 'field_therapy_coag_inr_corr':
    return array (
      'type' => 'medical_dosage_field',
      'instance' => 
      array (
        'label' => 'korrekctió történt-e',
        'widget' => 
        array (
          'weight' => '20',
          'type' => 'medical_dosage_widget',
        ),
        'settings' => 
        array (
          'unit_options' => "g\nmg",
          'dosage_type' => 'dose',
        ),
      ),
    );
  case 'field_therapy_coag_tct':
    return _opr2fld('number_decimal', 'TCT érték');
  case 'field_therapy_coag_tct_corr':
    return array (
      'type' => 'medical_dosage_field',
      'instance' => 
      array (
        'label' => 'korrekctió történt-e',
        'widget' => 
        array (
          'weight' => '20',
          'type' => 'medical_dosage_widget',
        ),
        'settings' => 
        array (
          'unit_options' => "g\nmg",
          'dosage_type' => 'dose',
        ),
      ),
    );
  case 'field_therapy_coag_hemo':
    return _opr2fld('boolean_radio', 'Ismert hemofilia');
  case 'field_therapy_coag_hemo_corr':
    return array (
      'type' => 'medical_dosage_field',
      'instance' => 
      array (
        'label' => 'korrekctió történt-e',
        'widget' => 
        array (
          'weight' => '20',
          'type' => 'medical_dosage_widget',
        ),
        'settings' => 
        array (
          'unit_options' => "g\nmg",
          'dosage_type' => 'dose',
        ),
      ),
    );

  case 'field_therapy_sed':
    return _opr2fld('boolean_radio', 'A. Szedáció');
  case 'field_therapy_sed_wake':
    return _opr2fld('boolean_radio', 'Éber szedáció');
  case 'field_therapy_sed_wake_what':
  case 'field_therapy_sed_adot_what':
    return _opr2fld('text', 'mi');
  case 'field_therapy_sed_wake_dose':
  case 'field_therapy_sed_other_dose':
  case 'field_therapy_sed_adot_dose':
    return _opr2fld('text', 'mennyi');
  case 'field_therapy_sed_prop':
    return _opr2fld('boolean_radio', 'Propofol szedáció');
  case 'field_therapy_sed_prop_dose':
    return _opr2fld('text', 'dózis');
  case 'field_therapy_sed_other':
    return _opr2fld('boolean_radio', 'Egyéb');
  case 'field_therapy_sed_other_what':
    return _opr2fld('boolean_radio', 'mi');
  case 'field_therapy_sed_mon':
    return _opr2fld('boolean_radio', 'Beteg monitorozása a beavatkozás alatt');
  case 'field_therapy_sed_mon_how':
    return _opr2fld('select_or_other', 'típusa', array(
      'options' => array(
        'pulzoxy' => 'pulzoxymetria',
        'ekg' => 'EKG',
        'bp' => 'vérnyomás',
      ),
    ));
  case 'field_therapy_sed_adot':
    return _opr2fld('boolean_radio', 'Antidotum alkalmazása');

  case 'field_therapy_pepp':
    $field = _opr2fld('boolean_radio', 'B. PEP gyógyszeres profilaxis');
    $field['instance']['description'] = 'PPS a 11. pontban a terápiás fejezetben!';
    return $field;
  case 'field_therapy_pepp_ind':
    return _opr2fld('boolean_radio', 'Indomethacin kúp');
  case 'field_therapy_pepp_ind_cc':
  case 'field_therapy_pepp_dic_cc':
    return _opr2fld('number_decimal', '100 mg /', array(
      'suffix' => 'mg',
    ));
  case 'field_therapy_pepp_ind_when':
  case 'field_therapy_pepp_dic_when':
  case 'field_therapy_pepp_other_when':
    return _opr2fld('number_list', 'mikor', array(
      'options' => array(
        1 => 'vizsgálat előtt',
        'vizsgálat után',
      ),
      'buttons' => TRUE,
    ));
  case 'field_therapy_pepp_dic':
    return _opr2fld('boolean_radio', 'Diclofenac kúp');
  case 'field_therapy_pepp_iv':
    return _opr2fld('boolean_radio', 'Infúzió ERCP után');
  case 'field_therapy_pepp_iv_what':
  case 'field_therapy_pepp_other_what':
    return _opr2fld('text', 'mi');
  case 'field_therapy_pepp_iv_dose':
  case 'field_therapy_pepp_other_dose':
    return _opr2fld('text', 'mennyi');
  case 'field_therapy_pepp_other':
    return _opr2fld('boolean_radio', 'Egyéb');
  case 'field_therapy_abpr':
    return _opr2fld('boolean_radio', 'C. Antibiotikus profilaxis');
  case 'field_therapy_abpr_type':
    return _opr2fld('number_list', 'típusa', array(
      'options' => array(
        1 => 'egyszeri dózis',
        'folyamatos kezelés',
      ),
      'buttons' => TRUE,
    ));
  case 'field_therapy_abpr_when':
    return _opr2fld('number_list', 'Terápia kezdete', array(
      'options' => array(
        1 => 'vizsgálat előtt',
        'vizsgálat után',
      ),
      'buttons' => TRUE,
    ));
  case 'field_therapy_abpr_dose':
    return _opr2fld('text', 'Antibiotikum dózisa');
  case 'field_therapy_abpr_len':
    return _opr2fld('text', 'Kezelés időtartama');

  case 'field_examtime_start':
    return _opr2fld('date_time', 'A. A vizsgálat kezdete');
  case 'field_examtime_can':
    return _opr2fld('hms', 'B. Kanülálás ideje natív papilla esetén');
  case 'field_examtime_duodiff':
    return _opr2fld('hms', 'Duodenoszkóp pozicionálása és szelektív kanülálás között eltelt idő');
  case 'field_examtime_end':
    return _opr2fld('date_time', 'C. A vizsgálat vége');
  case 'field_examtime_fluo':
    return _opr2fld('hms', 'D. Fluoroscopia ideje');
  case 'field_examtime_fluo_dose':
    return _opr2fld('number_decimal', 'Sugár dózis', array(
      'suffix' => 'Gy',
    ));

  case 'field_anatomy_opstop':
    return _opr2fld('number_list', 'Operált gyomor', array(
      'options' => array(
        1 => 'BI',
        2 => 'BII',
        'total gastrectomia',
        'Whipple',
        'Roux-en-Y bariatriai sebészet',
      ),
    ));
  case 'field_anatomy_deform':
    return _opr2fld('number_list', 'Deformáltság / szűkület', array(
      'options' => array(
        1 => 'pylorus',
        'bulbus',
        'postbulbaris duodenum',
      ),
    ));
  case 'field_anatomy_vaterpap':
    return _opr2fld('select_or_other', 'Vater papilla és orificium', array(
      'options' => array(
        'normal' => 'Normál',
        'orifitium lacer' => 'Lacerált orifitium',
        'fistula' => 'Fistula',
        'papilla stone' => 'Papillába ékelt kő',
        'neoplasia' => 'Neoplasia',
      ),
    ));
  case 'field_anatomy_pastest':
    return _opr2fld('boolean_radio', 'Korábbi EST');
  case 'field_anatomy_pastest_res':
    return _opr2fld('number_list', 'eredménye', array(
      'options' => array(
        1 => 'jó tágasságú',
        2 => 'beszűkült',
      ),
    ));
  case 'field_anatomy_juxtapdiv':
    return _opr2fld('boolean_radio', 'Juxtapapillaris diverticulum');
  case 'field_anatomy_orivis':
    return _opr2fld('boolean_radio', 'Orifitium látható');
  case 'field_anatomy_pappos':
    return _opr2fld('number_list', 'Papillaris traktus helyzete', array(
      'options' => array(
        1 => 'diverticulum peremén',
        2 => 'diverticulumban',
      ),
    ));
  case 'field_anatomy_sampling':
    return _opr2fld('boolean_radio', 'Mintavétel');

  case 'field_cannul_biliary':
    return _opr2fld('boolean_radio', 'A. Epeúti kanülálás');
  case 'field_cannul_pancmaj':
    return _opr2fld('boolean_radio', 'B. Pancreasvezeték kanülálás major papillán');
  case 'field_cannul_pancmin':
    return _opr2fld('boolean_radio', 'C. Pancreasvezeték kanülálás minor papillán');

  case 'field_cannul_biliary_sup':
  case 'field_cannul_pancmaj_sup':
  case 'field_cannul_pancmin_sup':
    return _opr2fld('boolean_radio', 'Felszínes kanülálás');
  case 'field_cannul_biliary_sup_type':
  case 'field_cannul_pancmaj_sup_type':
  case 'field_cannul_pancmin_sup_type':
    return _opr2fld('number_list', 'típusa', array(
      'options' => array(
        1 => 'vezetődróttal',
        'papillotommal',
        'kanüllel',
        'kontraszt',
      ),
    ));
  case 'field_cannul_biliary_deep':
  case 'field_cannul_pancmaj_deep':
  case 'field_cannul_pancmin_deep':
    return _opr2fld('boolean_radio', 'Mély kanülálás');
  case 'field_cannul_biliary_deep_type':
  case 'field_cannul_pancmaj_deep_type':
  case 'field_cannul_pancmin_deep_type':
    return _opr2fld('number_list', 'típusa', array(
      'options' => array(
        1 => 'vezetődróttal',
        'papillotommal',
        'kanüllel',
      ),
    ));
  case 'field_cannul_biliary_precut':
  case 'field_cannul_pancmaj_precut':
  case 'field_cannul_pancmin_precut':
    return _opr2fld('boolean_radio', 'Precut papillotomia');
  case 'field_cannul_biliary_precut_type':
  case 'field_cannul_pancmaj_precut_type':
  case 'field_cannul_pancmin_precut_type':
    return _opr2fld('number_list', 'típusa', array(
      'options' => array(
        1 => 'papillotommal',
        'tűkéssel orifitiumból',
        'tűkéssel fistulotomia',
      ),
    ));
  case 'field_cannul_biliary_pwir':
    return _opr2fld('boolean_radio', 'Pancreasvezetékbe helyezett vezetődrót mellett');
  case 'field_cannul_biliary_pwir_type':
    return _opr2fld('number_list', 'típusa', array(
      'options' => array(
        1 => 'vezetődróttal',
        'papillotommal',
        'kanüllel',
      ),
    ));
  case 'field_cannul_biliary_pst':
    return _opr2fld('boolean_radio', 'Pancreasvezetékbe helyezett stent mellett');
  case 'field_cannul_biliary_pst_type':
    return _opr2fld('number_list', 'típusa', array(
      'options' => array(
        1 => 'vezetődróttal',
        'papillotommal',
        'kanüllel',
      ),
    ));
  case 'field_cannul_biliary_rendez':
    return _opr2fld('boolean_radio', 'Randevú technika');
  case 'field_cannul_biliary_fail':
    return _opr2fld('boolean_radio', 'Sikertelen (epevezeték nem ábrázolódik)');
  case 'field_cannul_pancmaj_fail':
  case 'field_cannul_pancmin_fail':
    return _opr2fld('boolean_radio', 'Sikertelen (pancreasvezeték nem ábrázolódik)');
  case 'field_cannul_biliary_pancduct':
    return _opr2fld('number_list', 'Pancreas vezeték ábrázolódott-e', array(
      'options' => array(
        1 => 'igen',
        2 => 'részlegesen',
        '0' => 'nem',
      ),
      'buttons' => TRUE,
    ));
  case 'field_cannul_biliary_pancins':
    return _opr2fld('number_list', 'Pancreas vezetékbe vezető / papillotom / kanül jutott', array(
      'options' => array(
        '0' => 'nem',
        1 => '1x',
        2 => 'többször',
      ),
      'buttons' => TRUE,
    ));
  case 'field_cannul_contrastex':
    return _opr2fld('select_or_other', 'F. Kontrasztanyag extravazáció', array(
      'options' => array(
        'submucose' => 'Szubmukózus',
      ),
    ));

  case 'field_cpgraph_bil':
    return _opr2fld('number_list', 'A. Epevezeték', array(
      'options' => array(
        '0' => 'Normál',
        1 => 'Szabálytalan',
      ),
      'buttons' => TRUE,
    ));
  case 'field_cpgraph_bil_dil':
  case 'field_cpgraph_panc_dil':
    return _opr2fld('boolean_radio', 'Tágulat');
  case 'field_cpgraph_bil_cal':
  case 'field_cpgraph_panc_cal':
    return _opr2fld('boolean_radio', 'Kaliber egyenetlenség');
  case 'field_cpgraph_bil_stone':
    return _opr2fld('boolean_radio', 'Epeúti kő');
  case 'field_cpgraph_bil_stone_size':
    return _opr2fld('number_decimal', 'méret', array(
      'suffix' => 'mm',
    ));
  case 'field_cpgraph_bil_stone_num':
    return _opr2fld('number_integer', 'szám');
  case 'field_cpgraph_bil_stone_loc':
    return _opr2fld('text', 'lokalizáció');
  case 'field_cpgraph_bil_sludge':
    return _opr2fld('boolean_radio', 'Sludge');
  case 'field_cpgraph_bil_pus':
    return _opr2fld('boolean_radio', 'Genny');
  case 'field_cpgraph_bil_sten':
    return _opr2fld('boolean_radio', 'Epevezeték szűkület');
  case 'field_cpgraph_bil_sten_loc':
    return _opr2fld('number_list', 'lokalizáció', array(
      'options' => array(
        1 => 'alsó',
        'kp',
        'felső harmad',
        'hiláris',
        'jobb vagy bal intrahepaticus',
      ),
    ));
  case 'field_cpgraph_bil_sten_len':
    return _opr2fld('number_decimal', 'hossz', array(
      'suffix' => 'mm',
    ));
  case 'field_cpgraph_bil_sten_mal':
  case 'field_cpgraph_panc_sten_mal':
    return _opr2fld('number_list', 'malignitás', array(
      'options' => array(
        1 => 'benignus',
        'malignus',
        '0' => 'ismeretlen természetű',
      ),
    ));
  case 'field_cpgraph_bil_sten_sam':
    return _opr2fld('number_list', 'mintavétel', array(
      'options' => array(
        1 => 'citológia',
        'biopszia',
        'leoltás',
      ),
    ));
  case 'field_cpgraph_bil_flux':
    return _opr2fld('boolean_radio', 'Epecsorgás');
  case 'field_cpgraph_bil_flux_loc':
    return _opr2fld('text', 'lokalizáció');

  case 'field_cpgraph_panc':
    return _opr2fld('number_list', 'B. Pancreas vezeték', array(
      'options' => array(
        '0' => 'Normál',
        1 => 'Szabálytalan',
      ),
      'buttons' => TRUE,
    ));
  case 'field_cpgraph_panc_wir':
    return _opr2fld('boolean_radio', 'Wirsungolithiasis');
  case 'field_cpgraph_panc_sten':
    return _opr2fld('boolean_radio', 'Szűkület');
  case 'field_cpgraph_panc_sten_loc':
    return _opr2fld('number_list', 'lokalizáció', array(
      'options' => array(
        1 => 'fej',
        'test',
        'farok',
      ),
    ));
  case 'field_cpgraph_panc_sten_len':
    return _opr2fld('number_decimal', 'hossz', array(
      'suffix' => 'mm',
    ));
  case 'field_cpgraph_panc_sten_sam':
    return _opr2fld('number_list', 'mintavétel', array(
      'options' => array(
        1 => 'citológia',
        'pancreas nedv',
      ),
    ));
  case 'field_cpgraph_panc_pcyst':
    return _opr2fld('boolean_radio', 'Pseudocysta telődés a vezeték felől');
  case 'field_cpgraph_panc_pcyst_loc':
    return _opr2fld('text', 'lokalizáció');
  case 'field_cpgraph_panc_pcyst_size':
    return _opr2fld('number_decimal', 'nagyság', array(
      'suffix' => 'mm',
    ));

  case 'field_therapy_sphin':
    return _opr2fld('boolean_radio', 'A. Sphincterotomia');
  case 'field_therapy_sphin_precut':
    return _opr2fld('boolean_radio', 'Precut papillotomia');
  case 'field_therapy_sphin_precut_type':
    return _opr2fld('number_list', 'típusa', array(
      'options' => array(
        1 => 'papillotommal',
        'tűkéssel orifitiumból',
        'tűkéssel fistulotomia',
      ),
    ));
  case 'field_therapy_sphin_reg':
    return _opr2fld('boolean_radio', 'Hagyományos');
  case 'field_therapy_sphin_repap':
    return _opr2fld('boolean_radio', 'Repapillotomia');
  case 'field_therapy_sphin_psph':
    return _opr2fld('boolean_radio', 'Pancreas szfinkerotómia');
  case 'field_therapy_sphin_dsph':
    return _opr2fld('boolean_radio', 'Kettős szfinkerotómia');
  case 'field_therapy_dil':
    return _opr2fld('boolean_radio', 'B. Dilatáció');
  case 'field_therapy_dil_pbal':
    return _opr2fld('boolean_radio', 'Papilla ballon dilatációja');
  case 'field_therapy_dil_pbal_size':
  case 'field_therapy_dil_sten_size':
    return _opr2fld('number_decimal', 'dilatáció mértéke', array(
      'suffix' => 'mm',
    ));
  case 'field_therapy_dil_pbal_bsize':
  case 'field_therapy_dil_sten_bsize':
    return _opr2fld('number_decimal', 'ballon méret', array(
      'suffix' => 'mm',
    ));
  case 'field_therapy_dil_sten':
    return _opr2fld('boolean_radio', 'Szűkület dilatáció');
  case 'field_therapy_dil_sten_type':
    return _opr2fld('number_list', 'típusa', array(
      'options' => array(
        1 => 'ballon',
        'bougie',
      ),
      'buttons' => TRUE,
    ));
  case 'field_therapy_stonex':
    return _opr2fld('boolean_radio', 'C. Kőextrakció');
  case 'field_therapy_stonex_dorm':
    return _opr2fld('boolean_radio', 'Dormia');
  case 'field_therapy_stonex_bal':
    return _opr2fld('boolean_radio', 'Ballon');
  case 'field_therapy_stonex_mlit':
    return _opr2fld('boolean_radio', 'Mechanikus lithotrypsia');
  case 'field_therapy_stonex_free':
    return _opr2fld('boolean_radio', 'Kőmentes az epevezeték');

  case 'field_proposal_iv':
    return _opr2fld('boolean_radio', 'infúzió');
  case 'field_proposal_iv_amount':
    return _opr2fld('number_decimal', 'mennyiség', array(
      'suffix' => 'ml',
    ));
  case 'field_proposal_peros':
    return _opr2fld('boolean_radio', 'per os táplálás');
  case 'field_proposal_peros_type':
    return _opr2fld('number_list', 'típusa', array(
      'options' => array(
        1 => 'normál',
        2 => 'zsírmentes',
        3 => 'csak folyadék',
      ),
      'buttons' => TRUE,
    ));
  case 'field_proposal_ab':
    return _opr2fld('boolean_radio', 'antibiotikum');
  case 'field_proposal_ab_type':
    return _opr2fld('select_or_other', 'típusa', array(
      'options' => array(
        'ceftriaxon' => 'Ceftriaxon',
        'ciprofloxacin' => 'Ciprofloxacin',
        'amoxicillin-clavacid' => 'Amoxicillin-klavulánsav',
      ),
    ));
  case 'field_proposal_acoag':
    return _opr2fld('boolean_radio', 'antikoaguláns / aggregáció gátló kezelés folytatása');
  case 'field_proposal_acoag_date':
    return _opr2fld('date_which_day', 'időpont');
  case 'field_proposal_lab':
    return _opr2fld('boolean_radio', 'labor kontroll');
  case 'field_proposal_lab_type':
    return _opr2fld('number_list', 'típusa', array(
      'options' => array(
        1 => 'vérkép',
        2 => 'amiláz',
        3 => 'CRP',
        4 => 'billirubin',
        5 => 'májenzimek',
      ),
      'multiple' => TRUE,
    ));
  case 'field_proposal_lab_date':
    return _opr2fld('date_which_day', 'időpont');
  case 'field_proposal_imag':
    return _opr2fld('boolean_radio', 'további képalkotó');
  case 'field_proposal_imag_type':
    return _opr2fld('number_list', 'típusa', array(
      'options' => array(
        1 => 'CT',
        2 => 'MR',
        3 => 'EUH',
      ),
      'multiple' => TRUE,
    ));
  case 'field_proposal_consil':
    return _opr2fld('boolean_radio', 'konzilium');
  case 'field_proposal_consil_type':
    return _opr2fld('number_list', 'típusa', array(
      'options' => array(
        1 => 'invazív radiológiai konzilium',
        2 => 'sebészeti konzilium',
      ),
      'multiple' => TRUE,
    ));

  case 'field_comp_immed':
    $field = _opr2fld('boolean_radio', 'A. Azonnali');
    $field['instance']['description'] = 'vizsgálat alatt illetve közvetlenül utána jelentkező';
    return $field;
  case 'field_comp_immed_bleed':
  case 'field_comp_late_bleed':
    return _opr2fld('boolean_radio', 'Vérzés');
  case 'field_comp_immed_bleed_hs':
  case 'field_comp_late_bleed_hs':
    return _opr2fld('select_or_other', 'Endoszkópos hemosztázis módja', array(
      'options' => array(
        'adrenaline' => 'adrenalin',
        'thermocoag' => 'thermocoagulatio',
        'clip' => 'klip',
      ),
      'multiple' => TRUE,
    ));
  case 'field_comp_immed_bleed_stop':
  case 'field_comp_late_bleed_stop':
    return _opr2fld('boolean_radio', 'Megszűnt');
  case 'field_comp_immed_perf':
  case 'field_comp_late_perf':
    return _opr2fld('boolean_radio', 'Perforáció');
  case 'field_comp_immed_perf_type':
  case 'field_comp_late_perf_type':
    return _opr2fld('number_list', 'típusa', array(
      'options' => array(
        1 => 'vezetődrót',
        2 => 'periampullaris',
        3 => 'ampullától távoli',
      ),
      'buttons' => TRUE,
    ));
  case 'field_comp_immed_perf_air':
  case 'field_comp_late_perf_air':
    return _opr2fld('number_list', 'Levegő', array(
      'options' => array(
        1 => 'retroperitonealis levegő',
        2 => 'intraperitonealis levegő',
      ),
      'buttons' => TRUE,
    ));
  case 'field_comp_immed_perf_ther':
  case 'field_comp_immed_carp_ther':
  case 'field_comp_late_perf_ther':
  case 'field_comp_late_chola_ther':
  case 'field_comp_late_chocy_ther':
    return _opr2fld('text', 'Terápia');
  case 'field_comp_immed_carp':
    return _opr2fld('boolean_radio', 'Kardiopulmonális');
  case 'field_comp_immed_carp_type':
    return _opr2fld('number_list', 'típusa', array(
      'options' => array(
        1 => 'hipotenzió',
        2 => 'ritmuszavar',
        3 => 'hipoxia',
      ),
      'buttons' => TRUE,
    ));
  case 'field_comp_late':
    $field = _opr2fld('boolean_radio', 'B. Késői');
    $field['instance']['description'] = 'vizsgálat után 2 héten belül jelentkező';
    return $field;
  case 'field_comp_late_bleed_det':
  case 'field_comp_late_perf_det':
  case 'field_comp_late_chola_det':
  case 'field_comp_late_chocy_det':
    return _opr2fld('date_which_day', 'Észlelés ideje');
  case 'field_comp_late_bleed_exam':
    return _opr2fld('date_which_day', 'Endoszkópos vizsgálat ideje');
  case 'field_comp_late_bleed_tf':
    return _opr2fld('boolean_radio', 'Transzfúziós igény');
  case 'field_comp_late_bleed_tf_type':
    return _opr2fld('text', 'típusa');
  case 'field_comp_late_bleed_tf_amt':
    return _opr2fld('number_decimal', 'mennyiség', array(
      'suffix' => 'E',
    ));
  case 'field_comp_late_panc':
    return _opr2fld('boolean_radio', 'Pancreatitis');
  case 'field_comp_late_panc_pep':
    return _opr2fld('boolean_radio', 'Volt-e korábban PEP');
  case 'field_comp_late_panc_sev':
    return _opr2fld('number_list', 'Súlyosság', array(
      'options' => array(
        1 => 'enyhe',
        2 => 'közepes',
        3 => 'súlyos',
      ),
      'buttons' => TRUE,
    ));
  case 'field_comp_late_chola':
    return _opr2fld('boolean_radio', 'Cholangitis');
  case 'field_comp_late_chocy':
    return _opr2fld('boolean_radio', 'Cholecystitis');
  case 'field_comp_after':
    return _opr2fld('boolean_radio', 'C. 30 napos utánkövetés');
  case 'field_comp_after_mort':
    return _opr2fld('boolean_radio', 'Mortalitás');
  case 'field_comp_after_mort_date':
    return _opr2fld('date_which_day', 'dátum');
  case 'field_comp_after_mort_conn':
    return _opr2fld('boolean_radio', 'összefüggésbe hozható-e a vizsgálattal (pl. PEP)');
  case 'field_comp_after_ther':
    return _opr2fld('number_list', 'Hazabocsátás után szükség volt-e ellátásra', array(
      'options' => array(
        1 => 'sürgősségi',
        2 => 'gasztroenterológiai',
        3 => 'sebészeti',
      ),
      'multiple' => TRUE,
    ));
  case 'field_comp_sev':
    $field = _opr2fld('number_list', 'D. A szövődmény súlyossága', array(
      'options' => array(
        1 => 'Enyhe',
        2 => 'Közepes',
        3 => 'Súlyos',
        4 => 'Fatális',
      ),
    ));
    $field['instance']['description'] =
      '<strong>Enyhe:</strong> a beavatkozás leállítását eredményezi, konzultációt igényel, 3 napon belüli kórházi kezelés<br>'.
      '<strong>Közepes:</strong> légzéstámogatás szüksége éber szedáció során, 4-10 napos kórházi kezelés, intenzív osztályon 1 napos ellátás, transzfúzió, ismételt endoszkópia, intervenciós radiológia<br>'.
      '<strong>Súlyos:</strong>10 napot meghaladó kórházi kezelés, több mint 1 napos intenzív osztályos kezelés, sebészet, tartós károsodás';
    return $field;

  case 'field_diff_obj':
    $field = _opr2fld('number_list', 'A. Objektív paraméterek alapján (módosított Schutz – ASGE)', array(
      'options' => array(
        1 => 'Grade 1',
        'Grade 2',
        'Grade 3',
        'Grade 4',
      ),
    ));
    $field['instance']['description'] = '<strong>Grade 1:</strong> Kívánt vezeték mély kanülálása; major papilla mintavétel; epeúti stent eltávolítás / csere<br>'.
      '<strong>Grade 2:</strong> Epeúti kőeltávolítás &lt; 10 mm; epecsorgás kezelése; extrahepaticus benignus és malignus szűkületek kezelése; profilaktikus pancreas stent<br>'.
      '<strong>Grade 3:</strong> Epeúti kőeltávolítás &gt; 10 mm; minor papilla kanülálás / terápia; proximálisan migrált stent eltávolítása; intraductalis képalkotás, biopszia, FNA; akut vagy rekurrens pancreatitis kezelése; pancreas szűkületek kezelése; pancreas kő eltávolítás &lt; 5 mm; hilaris tumorok kezelése; benignus epeúti szűkületek kezelése hilusban vagy intrahepaticusan; SOD <br>'.
      '<strong>Grade 4:</strong> Proximalisan migrált pancreas stent eltávolítása; intraductalis terápia; pancreas kő eltávolítás, impaktált és/vagy &gt; 5 mm; intrahepaticus kövek; pseudocysta drainage, necrosectomia; ampullectomia, Whipple vagy Roux-en-Y bariatriai sebészet utáni ERCP';
    return $field;
  case 'field_diff_subj':
    $field = _opr2fld('number_list', 'B. Szubjektív megítélés', array(
      'min' => 1,
      'max' => 10,
    ));
    $field['instance']['description'] = '1: nagyon könnyű – 10: nagyon nehéz';
    return $field;
  }
}

